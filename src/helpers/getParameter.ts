import { SSM } from 'aws-sdk'

export default (ssm: SSM, ssmParams: SSM.Types.GetParameterRequest): Promise<string> =>
  ssm.getParameter(ssmParams).promise()
    .then(({ Parameter }) =>
      !Parameter
        ? Promise.reject(new Error('SSM parameter not found!'))
        : Parameter.Value === undefined
          ? Promise.reject(new Error('SSM parameter undefined!'))
          : Parameter.Value)
