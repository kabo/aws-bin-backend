import { S3, SSM } from 'aws-sdk'
import Rcond from 'ramda/src/cond'
import Requals from 'ramda/src/equals'
import RifElse from 'ramda/src/ifElse'
import Romit from 'ramda/src/omit'
import Rpipe from 'ramda/src/pipe'
import RpropSatisfies from 'ramda/src/propSatisfies'
import Rtap from 'ramda/src/tap'
import RT from 'ramda/src/T'
import Runless from 'ramda/src/unless'
import { v4 as uuidv4 } from 'uuid'
import getParameter from '../helpers/getParameter'

interface IRequest {
  readonly length: number
  readonly password: string
}
interface ISignRequest {
  readonly ContentLength: number
  readonly Key: string
}

const
  { Bucket, Prefix, PasswordPath } = process.env,
  ssm = new SSM(),
  s3 = new S3(),
  lt = (x: number) => (y: number) => y < x,
  passthrough = (fn: (x: any) => Promise<any>) => (data: any) => fn(data).then(() => data),
  checkPassword = passthrough((req: IRequest) =>
    getParameter(ssm, { Name: PasswordPath!, WithDecryption: true })
      .then(Runless(
        Requals(req.password),
        () => Promise.reject('Invalid password')
      ))
  ),
  checkSize = passthrough(RifElse(
    RpropSatisfies(lt(512000), 'length'),
    () => Promise.resolve(),
    () => Promise.reject('File too big')
  )),
  addKey = ({ length }: IRequest): ISignRequest => ({ ContentLength: length, Key: `${Prefix}${uuidv4()}` }),
  generateSignedUrl = ({ Key }: ISignRequest) =>
    s3.getSignedUrlPromise('putObject', {
      Bucket,
      Key,
      CacheControl: 'no-cache',
      // ContentLength,
      ContentType: 'application/octet-stream',
      Expires: 120,
    }
    ),
  handleInvalidPassword = (message: string) => {
    console.warn(message) // eslint-disable-line fp/no-unused-expression
    return { statusCode: 403, body: JSON.stringify({ message }) }
  },
  handleFileTooBig = (message: string) => {
    console.warn(message) // eslint-disable-line fp/no-unused-expression
    return { statusCode: 400, body: JSON.stringify({ message }) }
  },
  handleOtherError = (e: any) => {
    console.error(e) // eslint-disable-line fp/no-unused-expression
    return { statusCode: 500, body: JSON.stringify({ message: 'An error occured' }) }
  }

export const handler = (event: any) =>
  Promise.resolve(event.body)
    .then(JSON.parse)
    .then(Rtap(Rpipe(Romit([ 'password' ]), console.log)))
    .then(checkPassword)
    .then(checkSize)
    .then(addKey)
    .then(generateSignedUrl)
    .then((url: string) => ({
      statusCode: 200,
      body: JSON.stringify({ url }),
    }))
    .catch(Rcond([
      [ Requals('Invalid password'), handleInvalidPassword ],
      [ Requals('File too big'), handleFileTooBig ],
      [ RT, handleOtherError ],
    ]))
