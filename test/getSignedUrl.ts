/* global describe, expect, it, jest */
/* eslint-disable better/explicit-return, fp/no-unused-expression, fp/no-nil */
import { handler } from '../src/handlers/getSignedUrl'

describe('getSignedUrl', () => {
  describe('handler', () => {
    it('handler should be a function', () => {
      expect(typeof handler).toBe('function')
    })
  })
})
