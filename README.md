# aws-bin-backend

## Prerequisties
* node 14

## Project Setup
* run ```yarn install``` in root of project

## Invoking functions
Locally
```yarn serverless invoke local -s test -f <functionName> -p <path to event.json>```

In AWS
```yarn serverless invoke -s test -f <functionName> -p <path to event.json>```
